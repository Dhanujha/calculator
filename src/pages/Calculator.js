import React, { Component } from 'react';
import Result from '../components/Result';
import KeyPad from '../components/KeyPad';
import "../App.css";
import { setExpression } from '../utils/utilities';
import { getExpression } from '../utils/utilities';


class Calculator extends Component {
    constructor(){
        super();
        this.state = {
            result: "",
        }
    }

    onClick = (button) => {
        if(button === "="){
            this.calculate()
        }else if(button === "C"){
            this.reset()
        }else if(button === "CE"){
            this.backspace()
        }else if(button === "save"){
            this.save()
        }else if(button === "show"){
            this.show()
        }else {
            this.setState({
                result: this.state.result + button  
            })
        }
    };

    calculate = () => {
        try{
            this.setState({
                result: this.state.result + "=" + eval(this.state.result)
            })
        }catch(err){
            this.setState({
                result : "error"
            })
        }
            
    };

    save = () => {
        setExpression(this.state.result);
    }

    show = () => {
        let exp = getExpression();
        this.setState({
            result : exp
        })
    }

    reset = () => {
        this.setState({
            result: ""
        })
    };

    backspace = () => {
        this.setState({
            result: this.state.result.slice(0, -1)
        })
    };

    render() {
        return (
            <div>
                <div className="calculator-body">
                    <Result result={this.state.result}/>
                    <KeyPad onClick={this.onClick}/>
                </div>
            </div>
        );
    }
}

export default Calculator;