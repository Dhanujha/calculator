import React from 'react';
import './App.css';
import { BrowserRouter as Router } from 'react-router-dom';
import { RoutedContent } from './routes';
const basePath = process.env.BASE_PATH;

function App() {
  return (
    <Router basename={ basePath }>
      <RoutedContent />
    </Router>
  );
}

export default App;
