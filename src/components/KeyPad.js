import React,{Component} from 'react';

const data = [["(","CE",")","C"],["7","8","9","/"],["4","5","6","*"],["1","2","3","-"],["0",".","=","+"],["save","show"]];

class KeyPad extends Component{
    render(){
        return(
            <div className="button">
                {data.map((item)=>{
                    return item.map((symbol) => {
                        return(
                            <button name={symbol} 
                             onClick={e => this.props.onClick(e.target.name)}
                            >{symbol}</button>
                        );
                    })
                })}
            </div>
        );
    }
}

export default KeyPad;