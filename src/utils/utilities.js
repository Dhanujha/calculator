import {EXPRESSION} from "./../config/Constants";

export const setExpression = expression => {
    localStorage.setItem(EXPRESSION,expression);
}

export const getExpression = () => {
    return localStorage.getItem(EXPRESSION);
}