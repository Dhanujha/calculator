import React from 'react';
import { Route, Switch } from 'react-router';
import Calculator from '../pages/Calculator'

export const RoutedContent = () => {
    return (
        <Switch>
            <Route path='/' component={Calculator} /> 
        </Switch>
    );
};
